﻿using APP_12._Interfaces;
using APP_12.Controllers.MyMaskot;
using APP_12.Models.Entities.MyMaskot;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace App_12.Test
{
    [TestFixture]
    class MyMaskotTest_Paciente
    {
        Mock<IMasterService<Paciente>> _pacientes;
        Mock<IMasterService<Especie>> _especies;
        Mock<IMasterService<Cliente>> _clientes;

        [SetUp]
        public void Inicial()
        {
            _pacientes = new Mock<IMasterService<Paciente>>();
            _especies = new Mock<IMasterService<Especie>>();
            _clientes = new Mock<IMasterService<Cliente>>();
        }

        [Test]
        public void ListaPost()
        {
            _pacientes.Setup(o => o.GetAll()).Returns( //It.IsAny<Persona>()
                new List<Paciente>()
                {
                    new Paciente {
                    },
                    new Paciente
                    {
                    }
                }
            );

            var controller = new PacienteController(_pacientes.Object, _especies.Object, _clientes.Object);

            var resultado = controller.Index() as ViewResult;
            Assert.AreEqual(2, (resultado.Model as List<Paciente>).Count());
        }

        [Test]
        public void GuardarPaciente_FechaFutura()
        {
            var controller = new PacienteController(_pacientes.Object, _especies.Object, _clientes.Object);
            controller.Create(
                new Paciente
                {
                    FechaRegistro= DateTime.Parse("2017/06/25")
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("FechaRegistro"));
        }
        [Test]
        public void GuardarPaciente_ClienteRequired()
        {
            var controller = new PacienteController(_pacientes.Object, _especies.Object, _clientes.Object);
            controller.Create(
                new Paciente
                {
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("ClienteId")); 
        }

        [Test]
        public void GuardarPaciente_NombreRequired()
        {
            var controller = new PacienteController(_pacientes.Object, _especies.Object, _clientes.Object);
            controller.Create(
                new Paciente
                {
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Nombre"));
        }

        [Test]
        public void GuardarPaciente_EspecieRequired()
        {
            var controller = new PacienteController(_pacientes.Object, _especies.Object, _clientes.Object);
            controller.Create(
                new Paciente
                {
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("EspecieId"));
        }

        [Test]
        public void GuardarPaciente_PesoMayorCero()
        {
            var controller = new PacienteController(_pacientes.Object, _especies.Object, _clientes.Object);
            controller.Create(
                new Paciente
                {
                    Peso = -1
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Peso"));
        }

        [Test]
        public void GuardarPaciente_DatosParticularesLength()
        {
            var controller = new PacienteController(_pacientes.Object, _especies.Object, _clientes.Object);
            controller.Create(
                new Paciente
                {
                    DatosParticulares = "La key ClientValidationEnabled en true nos permite trabajar con la validación del lado del cliente de manera predetermina. La key UnobtrusiveJavaScriptEnable en true nos evitara mezclar el código javascript dentro del HTML, tal como se explico en este post."
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("DatosParticulares"));
        }

    }

    [TestFixture]
    class MyMaskotTest_Consulta
    {
        Mock<IMasterService<Paciente>> _pacientes;
        Mock<IMasterService<Especie>> _especies;
        Mock<IMasterService<Cliente>> _clientes;
        Mock<IMasterService<Consulta>> _consultas;

        [SetUp]
        public void Inicial()
        {
            _pacientes = new Mock<IMasterService<Paciente>>();
            _especies = new Mock<IMasterService<Especie>>();
            _clientes = new Mock<IMasterService<Cliente>>();
            _consultas = new Mock<IMasterService<Consulta>>();
        }

        [Test]
        public void ListaConsultas()
        {
            
            //_consultas.Setup(o => o.Find(x=>x.PacienteId==1)).Returns( //It.IsAny<Persona>()
            _consultas.Setup(o => o.Find(It.IsAny<Expression<Func<Consulta, bool>>>())).Returns( //It.IsAny<Persona>()
                new List<Consulta>()
                {
                    new Consulta {
                    },
                    new Consulta
                    {
                    }
                }
            );

            var controller = new ConsultaController(
                _pacientes.Object, 
                _especies.Object, 
                _clientes.Object,
                _consultas.Object);

            var resultado = controller.GetByPacienteId(1) as ViewResult;
            Assert.AreEqual(2, (resultado.Model as List<Consulta>).Count());
        }

        [Test]
        public void GuardarConsulta_FechaFutura()
        {
            var controller = new ConsultaController(
                _pacientes.Object,
                _especies.Object,
                _clientes.Object,
                _consultas.Object);

            controller.Create(
                new Consulta
                {
                    Fecha = DateTime.Parse("2017/06/25")
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Fecha"));
        }

        [Test]
        public void GuardarConsulta_MotivoRequired()
        {
            var controller = new ConsultaController(
                _pacientes.Object,
                _especies.Object,
                _clientes.Object,
                _consultas.Object);

            controller.Create(
                new Consulta
                {
                    
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Motivo"));
        }

        [Test]
        public void GuardarPaciente_PesoMayorCero()
        {
            var controller = new ConsultaController(
                _pacientes.Object,
                _especies.Object,
                _clientes.Object,
                _consultas.Object);

            controller.Create(
                new Consulta
                {
                    Peso = -1
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Peso"));
        }
        [Test]
        public void GuardarPaciente_ComentarioLength()
        {
            var controller = new ConsultaController(
                _pacientes.Object,
                _especies.Object,
                _clientes.Object,
                _consultas.Object);

            controller.Create(
                new Consulta
                {
                    Comentarios = "La key ClientValidationEnabled en true nos permite trabajar con la validación del lado del cliente de manera predetermina. La key UnobtrusiveJavaScriptEnable en true nos evitara mezclar el código javascript dentro del HTML, tal como se explico en este post."

                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Comentarios"));
        }

              
    }
}
