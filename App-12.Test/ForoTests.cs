﻿using APP_12._Interfaces;
using APP_12.Controllers.Foro;
using APP_12.Models.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace App_12.Test
{
    //[TestFixture]
    class ForoTests
    {
        Mock<IMasterService<Post>> _posts;
        Mock<IMasterService<Categoria>> _categorias;

        //[SetUp]
        public void Inicial()
        {
            _posts = new Mock<IMasterService<Post>>();
            _categorias = new Mock<IMasterService<Categoria>>();
        }

        //[Test]
        public void ListaPost()
        {
            _posts.Setup(o => o.GetAll()).Returns( //It.IsAny<Persona>()
                new List<Post>()
                {
                    new Post {
                        PostId=1,
                        Titulo = "Ocio en Cajamarca",
                        CategoriaId=1,
                        Contenido="Orlando Games y Billar"
                    },
                    new Post
                    {
                        PostId = 2,
                        Titulo = "Educación en Cuzco",
                        CategoriaId = 2,
                        Contenido = "Amigos, es bueno estudiar"
                    }
                }
            );

            var controller = new PostController(_posts.Object, _categorias.Object);

            var resultado = controller.Index() as ViewResult;
            Assert.AreEqual(2, (resultado.Model as List<Post>).Count());
        }

        //[Test]
        public void GuardarExitoso()
        {
            var controller = new PostController(_posts.Object, _categorias.Object);
            controller.Create(new Post { CategoriaId=1,Titulo="We",Contenido="WA",PostId=100});
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        //[Test]
        public void GuardarFallido()
        {
            /*var impFalsa = new Mock<IPostService>();

            var controller = new ForoController(impFalsa.Object);
            controller.Create(new Post());
            controller.ViewData.ModelState.AddModelError("Titulo", "Nombre muy feo para el post");
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Titulo"));*/


            var controller = new PostController(_posts.Object, _categorias.Object);
            controller.Create(new Post {});
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

    }
}
