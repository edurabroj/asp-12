﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace APP_12.Models.Entities.MyMaskot
{
    public class Consulta
    {
        public int ConsultaId { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }
        public string Motivo { get; set; }
        public int Peso { get; set; }
        public string Comentarios { get; set; }
        public int PacienteId { get; set; }
        public Paciente Paciente { get; set; }
    }
}