﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APP_12.Models.Entities.MyMaskot.Enum
{
    public enum Sexo
    {
        Macho,
        Hembra,
        NoDefinido
    }
}