﻿using APP_12.Models.Entities.MyMaskot.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace APP_12.Models.Entities.MyMaskot
{
    public class Paciente
    {
        public int PacienteId { get; set; }
        //[Required]
        public int NroRegistro { get; set; }
        //[Required]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaRegistro { get; set; }
        //[Required]
        public int ClienteId { get; set; }
        public Cliente Cliente { get; set; }
        //[Required]
        public string Nombre { get; set; }
        public Sexo Sexo { get; set; }
        //[Required]
        public int EspecieId { get; set; }
        public Especie Especie { get; set; }

        public int Peso { get; set; }
        public string DatosParticulares { get; set; }
    }
}