﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APP_12.Models.Entities.MyMaskot
{
    public class Especie
    {
        public int EspecieId { get; set; }
        public string Name { get; set; }
    }
}