﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APP_12.Models.Entities
{
    public class Categoria
    {
        public int CategoriaId { get; set; }
        public string Nombre { get; set; }
    }
}