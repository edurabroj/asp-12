﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APP_12.Models.Entities
{
    public class Comentario
    {
        public int ComentarioId { get; set; }
        public string Texto { get; set; }
        public Post Post { get; set; }
        public int PostId { get; set; }
    }
}