﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace APP_12.Models.Entities
{
    public class Post
    {
        public int PostId { get; set; }
        public Categoria Categoria { get; set; }
        [Required]
        public int CategoriaId { get; set; }
        [Required]
        public string Titulo { get; set; }
        [Required]
        public string Contenido { get; set; }
    }
}