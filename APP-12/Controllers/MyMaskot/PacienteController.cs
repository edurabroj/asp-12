﻿using APP_12._Interfaces;
using APP_12.Models.Entities.MyMaskot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APP_12.Controllers.MyMaskot
{
    public class PacienteController : Controller
    {
        IMasterService<Paciente> _pacientes;
        IMasterService<Especie> _especies;
        IMasterService<Cliente> _clientes;

        public PacienteController(
            IMasterService<Paciente> pacientes,
            IMasterService<Especie> especies,
            IMasterService<Cliente> clientes)
        {
            _pacientes = pacientes;
            _especies = especies;
            _clientes = clientes;
        }

        // GET: Paciente
        public ActionResult Index()
        {
            return View(_pacientes.GetAll());
        }

        public ActionResult Create()
        {
            var especies = _especies.GetAll();
            ViewBag.Especies = new SelectList(especies, "EspecieId", "Name");
            var clientes = _clientes.GetAll();
            ViewBag.Clientes = new SelectList(clientes, "ClienteId", "FullName");

            var nroRegistro = _pacientes.GetAll().Count() > 0 ? _pacientes.GetAll().Last().NroRegistro + 1 : 1;

            return View(
                new Paciente {
                    NroRegistro = nroRegistro,
                    FechaRegistro = DateTime.Now
                }
            );
        }

        [HttpPost]
        public ActionResult Create(Paciente model)
        {
            var txtRequerido = "Campo Requerido";

            //Nro Registro
            if (model.NroRegistro<1)
                ViewData.ModelState.AddModelError("NroRegistro", "Mayor que 0");

            if (string.IsNullOrEmpty(model.NroRegistro.ToString()))
                ViewData.ModelState.AddModelError("NroRegistro", txtRequerido);

            if (_pacientes.Find(x=>x.NroRegistro==model.NroRegistro).Count()>0)
                ViewData.ModelState.AddModelError("NroRegistro", "Ya existe");

            //FechaRegistro
            if (model.FechaRegistro>DateTime.Now)
                ViewData.ModelState.AddModelError("FechaRegistro", "Fecha futura");

            if (string.IsNullOrEmpty(model.FechaRegistro.ToString()))
                ViewData.ModelState.AddModelError("FechaRegistro", txtRequerido);

            //NombreCliente
            if (model.ClienteId <= 0/* || string.IsNullOrEmpty(model.ClienteId.ToString())*/)
                ViewData.ModelState.AddModelError("ClienteId", txtRequerido);

            //NombrePaciente
            if (string.IsNullOrEmpty(model.Nombre))
                ViewData.ModelState.AddModelError("Nombre", txtRequerido);

            //Especie
            if (model.ClienteId <= 0 /*string.IsNullOrEmpty(model.EspecieId.ToString())*/)
                ViewData.ModelState.AddModelError("EspecieId", txtRequerido);

            //Peso
            if(model.Peso<=0)
                ViewData.ModelState.AddModelError("Peso", "Debe ser mayor que 0");

            //DatosPar
            if (!string.IsNullOrEmpty(model.DatosParticulares) && model.DatosParticulares.Length > 200)
                ViewData.ModelState.AddModelError("DatosParticulares", "Demasiado largo");

            if (ModelState.IsValid)
            {
                _pacientes.Add(model);
                return RedirectToAction("Index");
            }

            var especies = _especies.GetAll();
            ViewBag.Especies = new SelectList(especies, "EspecieId", "Name");
            var clientes = _clientes.GetAll();
            ViewBag.Clientes = new SelectList(clientes, "ClienteId", "FullName");
            return View(model);
        }
    }
}