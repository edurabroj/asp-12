﻿using APP_12._Interfaces;
using APP_12.Models.Entities.MyMaskot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APP_12.Controllers.MyMaskot
{
    public class ConsultaController : Controller
    {
        IMasterService<Paciente> _pacientes;
        IMasterService<Especie> _especies;
        IMasterService<Cliente> _clientes;
        IMasterService<Consulta> _consultas;

        public ConsultaController(
            IMasterService<Paciente> pacientes,
            IMasterService<Especie> especies,
            IMasterService<Cliente> clientes,
            IMasterService<Consulta> consultas)
        {
            _pacientes = pacientes;
            _especies = especies;
            _clientes = clientes;
            _consultas = consultas;
        }

        // GET: Paciente
        public ActionResult GetByPacienteId(int pacienteId)
        {
            return View(_consultas.Find(x=>x.PacienteId==pacienteId));
        }

        public ActionResult Create(int pacienteId)
        {
            return View(
                new Consulta
                {
                    Fecha = DateTime.Now,
                    PacienteId = pacienteId
                }
            );
        }

        [HttpPost]
        public ActionResult Create(Consulta model)
        {
            var txtRequerido = "Campo Requerido";

            //FechaRegistro
            if (model.Fecha > DateTime.Now)
                ViewData.ModelState.AddModelError("Fecha", "Fecha no válida");

            if (string.IsNullOrEmpty(model.Fecha.ToString()))
                ViewData.ModelState.AddModelError("Fecha", txtRequerido);

            //motivo
            if (string.IsNullOrEmpty(model.Motivo))
                ViewData.ModelState.AddModelError("Motivo", txtRequerido);

            //peso
            if (model.Peso <= 0)
                ViewData.ModelState.AddModelError("Peso", "Inválido");

            //DatosPar
            if (!string.IsNullOrEmpty(model.Comentarios))
                if(model.Comentarios.Length > 200)
                    ViewData.ModelState.AddModelError("Comentarios", "Demasiado largo");
            
            if (ModelState.IsValid)
            {
                if (model.Peso > 0)
                {
                    var paciente = _pacientes.GetById(model.PacienteId);
                    paciente.Peso = model.Peso;
                    _pacientes.Edit(paciente);
                }

                _consultas.Add(model);
                return RedirectToAction("Index","Paciente");
            }
            
            return View(model);
        }
    }
}