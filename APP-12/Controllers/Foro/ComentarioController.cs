﻿using APP_12._Interfaces;
using APP_12.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APP_12.Controllers.Foro
{
    public class ComentarioController : Controller
    {
        IMasterService<Comentario> _comentarios;

        public ComentarioController(IMasterService<Comentario> comentarios)
        {
            _comentarios = comentarios;
        }

        public ActionResult GetByPostId(int postId)
        {
            return View(_comentarios.Find(x=>x.PostId==postId));
        }

        public ActionResult Create(int postId)
        {
            return View(new Comentario { PostId = postId});
        }

        [HttpPost]
        public ActionResult Create(Comentario model)
        {
            if (ModelState.IsValid)
            {
                _comentarios.Add(model);
                return RedirectToAction("Index","Post");
            }
            return View(model);
        }
    }
}