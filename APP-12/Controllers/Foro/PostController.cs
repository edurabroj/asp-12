﻿using APP_12._Interfaces;
using APP_12.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APP_12.Controllers.Foro
{
    public class PostController : Controller
    {
        IMasterService<Post> _posts;
        IMasterService<Categoria> _categorias;

        public PostController(
            IMasterService<Post> posts,
            IMasterService<Categoria> categorias)
        {
            _posts = posts;
            _categorias = categorias;
        }

        // GET: Post
        public ActionResult Index()
        {
            return View(_posts.GetAll());
        }
        
        public ActionResult Create()
        {
            var categorias = _categorias.GetAll();
            ViewBag.Categorias = new SelectList(categorias, "CategoriaId", "Nombre");

            return View();
        }

        [HttpPost]
        public ActionResult Create(Post model)
        {
            if(string.IsNullOrEmpty(model.Contenido))
                this.ViewData.ModelState.AddModelError("Contenido", "No puede ser nulo.");
            
            if (ModelState.IsValid)
            {
                _posts.Add(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}