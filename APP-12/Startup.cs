﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(APP_12.Startup))]
namespace APP_12
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
