namespace APP_12.Migrations
{
    using Models.Entities;
    using Models.Entities.MyMaskot;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<APP_12._Context.Contexto>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(APP_12._Context.Contexto context)
        {
            context.Clientes.AddOrUpdate(
              p => p.FullName,
              new Cliente
              {
                  ClienteId = 1,
                  FullName = "Eusebio Marrufo"
              },
              new Cliente
              {
                  ClienteId = 2,
                  FullName = "Naiby Man"
              }
            );

            context.Especies.AddOrUpdate(
              p => p.Name,
              new Especie
              {
                  EspecieId = 1,
                  Name = "Criollo"
              },
              new Especie
              {
                  EspecieId = 2,
                  Name = "Siete Razas"
              },
              new Especie
              {
                  EspecieId = 2,
                  Name = "Ramiro"
              }
            );

            /*context.Pacientes.AddOrUpdate(
            p => p.NroRegistro,
            new Paciente
            {

            }
            );*/

            /*
            context.Categorias.AddOrUpdate(
             p => p.Nombre,
             new Categoria { CategoriaId = 1, Nombre = "Ocio" },
             new Categoria { CategoriaId = 2, Nombre = "Educaci�n" }
           );

            context.Posts.AddOrUpdate(
              p => p.Contenido,
              new Post
              {
                  PostId = 1,
                  Titulo = "Ocio en Cajamarca",
                  CategoriaId = 1,
                  Contenido = "Orlando Games y Billar"
              },
              new Post
              {
                  PostId = 2,
                  Titulo = "Educaci�n en Cuzco",
                  CategoriaId = 2,
                  Contenido = "Amigos, es bueno estudiar"
              }
            );

            context.Comentarios.AddOrUpdate(
              p => p.Texto,
              new Comentario
              {
                  ComentarioId = 1,
                  PostId = 1,
                  Texto = "Tambi�n jugar f�tbol pe"
              },
              new Comentario
              {
                  ComentarioId = 2,
                  PostId = 1,
                  Texto = "<script>alert('hola')</script>"
              },
              new Comentario
              {
                  ComentarioId = 3,
                  PostId = 2,
                  Texto = "En youtebe hay tutoriales"
              },
              new Comentario
              {
                  ComentarioId = 4,
                  PostId = 2,
                  Texto = "<script>alert('educacion')</script>"
              }
            );*/
        }
    }
}
