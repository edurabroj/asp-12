﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace APP_12._Interfaces
{
    public interface IMasterService<TEntity> where TEntity : class

    {
        IQueryable<TEntity> PerformInclusions(IEnumerable<Expression<Func<TEntity, object>>> includeProperties,
                                                      IQueryable<TEntity> query);

        IQueryable<TEntity> AsQueryable();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includeProperties);
        IEnumerable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] includeProperties);
        TEntity GetById(Int32? id);
        IEnumerable<TEntity> SearchByCriteria(Expression<Func<TEntity, bool>> criterio);

        void Add(TEntity entidad);
        void Remove(Int32 id);
        void Commit();
        void Edit(TEntity entity);
    }
}
